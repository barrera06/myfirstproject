const users = [
    {
        "name": "Alex",
        "picture": "https://image.ibb.co/cA2oOb/alex_1.jpg",
    },
    {
        "name": "Bob",
        "picture": "https://image.ibb.co/gSyTOb/bob_1.jpg",
    },
    {
        "name": "Luke",
        "picture": "https://image.ibb.co/jOzeUG/luke_1.jpg",
    },
    {
        "name": "Bane",
        "picture": "https://image.ibb.co/cBZPww/bane_1.jpg",
    },
    {
        "name": "Darth Vader",
        "picture": "https://image.ibb.co/j4Ov3b/darth_vader_1.png",
    },
    {
        "name": "Zach",
        "picture": "https://image.ibb.co/b4kxGw/zach_1.jpg",
    },
    {
        "name": "Some",
        "picture": "url_1",
    },
    {
        "name": "Some",
        "picture": "url_2",
    }
]

const container_users = document.getElementById('list-users')

const showUsers = () => {
    users.forEach( user => {
        container_users.innerHTML += 
        `
        <div class="list-group">
            <label class="list-group-item">
                <img src="${user.picture}" class="rounded-circle" width="50">
                ${user.name}
            </label>
        </div>
        `
    })
}

showUsers()

